// [SECTION] JS Server
const express = require("express");
// mongoose is a package/module that allows the creation of schemas to model our data structures and also has access to different methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 4001;

// [SECTION] MongoDB Connection
// { newUrlParser : true } allows us to avoid any current and future errors while connecting to MongoDB
// Syntax
	// mongoose.connect("<MongoDB Atlas connection string>", { useNewUrlParser : true });
mongoose.connect("mongodb+srv://admin:admin1234@b256-biscocho.lbp0f9x.mongodb.net/B256_to-do?retryWrites=true&w=majority", {
		useNewUrlParser: true,
	useUnifiedTopology: true
});

// Checking of connection
// Allows to handle errors when the initial connection is establised
// Works with the on and once Mongoose methods
let db = mongoose.connection;

// If a connection error occurred, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log(`Were connected to the cloud database`));

// [SECTION] Mongoose Schema
// Schemas determine the structure of the document to be written in the database
// In laymans term, it acts like a blueprint of our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema
const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	name: String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value
		default: "pending"
	}
});

// [SECTION] Models
// Uses schemas and are used to create/instantiate objects that correspond to the schema
// Models use Schemas and they act as the middleman from the server (JS code) to our database
// Server > Schema (blueprint) > Database > Collection

// Models must be in singular form and capitalized
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection in postman
const Task = mongoose.model("Task", taskSchema);

// Middlewares
app.use(express.json());
// The url, by default, accepts only string and array data types. By extending the urlencoded, the app can now accept object data type as well.
app.use(express.urlencoded({extended: true}));

// Creating a new Task
/*
	Business Logic:
		1. Add a functionality to check if there are duplicate tasks
			- If the task already exists in the database, we return an error
			- If the task doesn't exist in the database, we add it in the database
		2. The task data will be coming from the request's body
		3. Create a new Task object with a "name" field/property
		4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
app.post("/task", (req, res) => {

	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria as a single object.
	// findOne() can send the possible result or error in another method called then() for further processing.
	// .then() is chained to another method/promise that is able to return a value or an error.
	// .then() waits for the previous method to complete its process. It will only run once the previous method is able to return a value or error.
	// .then() method can then process the returned result or error in a callback method inside it.
	// the callback method in the then() will be able to receive the result or error returned by the previous method it is attached to.
	// the .findOne() method returns the result first and the error second as parameters.
	// Call back functions in mongoose methods are programmed this way to store the returned results in the first parameter and any errors in the second parameter
	// If there are no matches, the value of result is null
	// "err" is a shorthand naming convention for errors
	Task.findOne({name: req.body.name}).then((result, err) => {

		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result !== null && result.name == req.body.name) {

			// Return a message to the client/Postman
			return res.send('Duplicate Task Found');

		// If no document was found
		} else {

			// Create a new task and save it to the database
			let newTask = new Task ({
				name: req.body.name
			});

			// The "save" method will store the information to the database
			// Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
			// The "save()" method can send the result or error in another JS method called then()
			// the .save() method returns the result first and the error second as parameters.
			newTask.save().then((savedTask, savedErr) => {

				if(savedErr) {

					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.error(savedErr);

				// No error found while creating the document
				} else {

					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("New Task Created");

				}
			})
		}
	})
})

// [SECTION] Getting All Tasks
// Business Logic
/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
app.get("/tasks", (req, res) => {


	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}).then((result, err) => {

		// If an error occurred
		if(err) {

			// Will print any errors found in the console
			return console.error(err)

		// If no errors are found
		} else {

			// Status "200" means that everything is "OK" in terms of processing
			// The "json" method allows to send a JSON format for the response
			// The returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
			res.status(200).json({
				data: result
			})		
		}
	})
})

///ACTIVITY

const createUsers = new mongoose.Schema({
	username: String,
	password: String
});

const user = mongoose.model("users", createUsers);
app.post("/signup", (req, res) => {
	user.findOne({username: req.body.username},{password: req.body.password}).then((result, err) =>
	{
	if(result !== null && result.name == req.body.name) {
		return res.send('Duplicate Username Found!');
	} else {
		let newUser = new user ({
			username: req.body.username,
			password: req.body.password
		});
		newUser.save().then((savedTask, savedErr) => {
			if(savedErr) {
				return console.error(savedErr);
			} else {
				return res.status(201).send("Registered Successfully");
			}
		})
	}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`));
